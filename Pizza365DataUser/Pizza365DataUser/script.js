




  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
   const gORDER_COL = ["orderCode","kichCo","loaiPizza","idLoaiNuocUong","thanhTien","hoTen","soDienThoai","trangThai","chitiet"]
var gDataUser = [];
var gUserPut = []
var gId =[];
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  $(document).ready(function(){
    onPageLoading();
    $("#user-table").on("click",".btn-detail",function() {
      var vOjbUserOrder = getDataUserFromButton(this)
      gId = vOjbUserOrder.id
         loadDataUserModal(vOjbUserOrder)
        $("#my-modal").modal("show");
    });
    //event click filter User
    $("#btn-fliter").click(function() {
      onBtnFilterUserClick()
    });
    //event click Confirmed
    $("#btn-confirm").click(function() {
      onBtnComfirmClick()
    });
    $("#btn-cancel").click(function() {
      onBtnCancleClick()
    })
  })
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
    //onload
    function onPageLoading() {
        getApiOrderUser()
        loadDataTableUser(gDataUser)
    };
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //load data table
  function dataTable() {
    var vTable = $("#user-table").DataTable({
        columns:[
            {data:gORDER_COL[0]},
            {data:gORDER_COL[1]},
            {data:gORDER_COL[2]},
            {data:gORDER_COL[3]},
            {data:gORDER_COL[4]},
            {data:gORDER_COL[5]},
            {data:gORDER_COL[6]},
            {data:gORDER_COL[7]},
            {data:gORDER_COL[8]},
        ],
        columnDefs:[
              {targets:[8],
               className:"text-center",
               defaultContent:`<button class= 'btn btn-primary  btn-detail'> Chi tiết</i></button>`
            }                
          ]   
    })
    return vTable
  };
  //put api status orderrlist
  function putStatusOrderList(paramStatus) {
    var vArrayIndex = getArrayUserById(gId);
    const baseUrl = "http://203.171.20.210:8080/devcamp-pizza365/orders/"  + gId;
    $.ajax({
      url: baseUrl,
      async:false,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(paramStatus),
      //B4: xử lý hiển thị
      success: function(res){
        console.log(res)
          gDataUser.splice(vArrayIndex,1, res);
          console.log("id User " + res.id)
      },
      error: function(ajaxContext){
          alert (ajaxContext.responseText);
      }
  });
 
  }
  // get api json Order User
  function getApiOrderUser() {
     var vBaseUrl = "http://203.171.20.210:8080/devcamp-pizza365/orders";
    $.ajax({
        url: vBaseUrl,
        async:false,
        type: "GET",
        success:function(data){
            //console.log(data)
            gDataUser = data;
            console.log(gDataUser)
        },
        error: function(ajaxContext){
            alert(ajaxContext.responseText)
        }
      })
    };
    //load data table
function loadDataTableUser(paramTeacherArray){
    var vDataTable = dataTable()
    vDataTable.clear();
    vDataTable.rows.add(paramTeacherArray);
    vDataTable.draw();
 };
  // this is get data userOrder by button 
function getDataUserFromButton(paramElement) {
    var vRowClick = $(paramElement).closest("tr"); // Xác định tr chứa nút bấm được click
    var vTable = $('#user-table').DataTable(); // tạo biện truy xuất đến datatable
    var vDataRow = vTable.row( vRowClick ).data(); // Lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
    console.log(vDataRow)
    return vDataRow
  };
  //this is event click btn Comfim
  function onBtnComfirmClick() {
    debugger
    var vObjStatus = {
         trangThai : "confirmed"
    }
   
      putStatusOrderList(vObjStatus)
      alert("update data success!");
      // B3 load data table
      loadDataTableUser(gDataUser);
     $("#my-modal").modal("hide");
    
  }
  //this is event click btn cancle
  function onBtnCancleClick() {
    var vObjStatus = {
         trangThai : "cancle"
    }
    if(gId !=""){
      putStatusOrderList(vObjStatus)
      // B3 load data table
      loadDataTableUser(gDataUser);
     $("#my-modal").modal("hide");
    }
  };
  //this is event click btn Fliter user
  function onBtnFilterUserClick(){
    // khai báo đối tượng để chứa dữ liệu trên form
    var vFilterObj = {
      trangThai: "",
      loaiPizza: ""
    };
    // B1 thu thập dữ liệu

    getFilterFormData(vFilterObj);
    console.log(vFilterObj)
    // B2 xử lý lọc dữ liệu.
    var vUsersResult = filterUsers(vFilterObj);
    console.log(vUsersResult)
    // B3 Hiển thị
    loadDataTableUser(vUsersResult);
  };
   // hàm thu thập dữ liệu lọc trên form
   function getFilterFormData(paramFilterObj) {
    paramFilterObj.trangThai = $("#inp-status-order").val().trim();
    paramFilterObj.loaiPizza = $("#inp-pizza").val().trim();
  };
   // hàm thực hiện lọc users trong mảng, và trả về mảng users sau khi lọc thỏa mãn điều kiện lọc
   function filterUsers(paramFilterObj) {
    var vUsers = gDataUser.filter( function(paramDataUser) {
      return ( removeVietnameseTones(paramDataUser.trangThai.toLowerCase()).includes(removeVietnameseTones(paramFilterObj.trangThai.toLowerCase())) || paramFilterObj.trangThai == "")
        && ( removeVietnameseTones(paramDataUser.loaiPizza.toLowerCase()).includes(removeVietnameseTones(paramFilterObj.loaiPizza.toLowerCase())) || paramFilterObj.loaiPizza == "");
    } );
    return vUsers;
  };
  // get array by id
function getArrayUserById(paramTypeId) {
  "use strict";
  var vNewIndex = "";
  var vIndex = 0;
  var vIsTypeFound = false;
  while(!vIsTypeFound && vIndex < gDataUser.length) {
    if(gDataUser[vIndex].id === paramTypeId) {
      vIsTypeFound = true;
      vNewIndex = vIndex;
    }
    else {
      vIndex ++;
    }
  }
  return vNewIndex;
}
  // hàm xóa bỏ dấu tiếng Việt
  function removeVietnameseTones(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
    str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
    str = str.replace(/đ/g,"d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    // Some system encode vietnamese combining accent as individual utf-8 characters
    // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
    str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
    // Remove extra spaces
    // Bỏ các khoảng trắng liền nhau
    str = str.replace(/ + /g," ");
    str = str.trim();
    // Remove punctuations
    // Bỏ dấu câu, kí tự đặc biệt
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
    return str;
};
// on load data user in form modal 
function loadDataUserModal(paramDataUser) {
   $("#int-orderid").val(paramDataUser.id);
   $("#select-combo").val(paramDataUser.kichCo).change();
   $("#int-duongkinh").val(paramDataUser.duongKinh);
   $("#int-suonnuong").val(paramDataUser.suon);
   $("#select-drink").val(paramDataUser.idLoaiNuocUong);
   $("#int-numberdrink").val(paramDataUser.soLuongNuoc);
   $("#int-vuocher").val(paramDataUser.idVourcher);
   $("#select-pizza").val(paramDataUser.loaiPizza);
   $("#int-salad").val(paramDataUser.salad);
   $("#int-price").val(paramDataUser.thanhTien);
   $("#int-discount").val(paramDataUser.giamGia);
   $("#int-fullname").val(paramDataUser.hoTen);
   $("#int-email").val(paramDataUser.email);
   $("#int-numberphone").val(paramDataUser.soDienThoai);
   $("#int-address").val(paramDataUser.diaChi);
   $("#int-message").val(paramDataUser.loiNhan);
   $("#int-status").val(paramDataUser.trangThai);
   $("#int-datecreated").val(paramDataUser.ngayCapNhat);
   $("#int-updateday").val(paramDataUser.ngayTao);
}

